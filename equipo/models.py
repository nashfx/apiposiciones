from django.db import models

# Create your models here.
class Equipo(models.Model):
    nombre = models.CharField(max_length=150)
    imagen = models.ImageField(upload_to='equipos/')
    nombreCorto = models.CharField(max_length=10)

    def __str__(self):
        return self.nombre

    def getNombreCorto(self):
        return self.nombreCorto
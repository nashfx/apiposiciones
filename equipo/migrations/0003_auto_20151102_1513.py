# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('equipo', '0002_auto_20151102_1511'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipo',
            name='imagen',
            field=models.ImageField(upload_to=b'equipos/'),
        ),
    ]

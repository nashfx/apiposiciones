# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('equipo', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='equipo',
            name='imagenUrl',
        ),
        migrations.AddField(
            model_name='equipo',
            name='imagen',
            field=models.ImageField(default=0, upload_to=b'media/'),
            preserve_default=False,
        ),
    ]

from django.contrib import admin

# Register your models here.
from partido.models import Partido

admin.site.register(Partido)
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('torneo', '0001_initial'),
        ('partido', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='partido',
            name='torneo',
            field=models.ForeignKey(related_name='torneoreferido', default=0, to='torneo.Torneo'),
            preserve_default=False,
        ),
    ]

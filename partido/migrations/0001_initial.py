# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('equipo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Partido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lugar', models.CharField(max_length=150)),
                ('fecha', models.DateField()),
                ('hora', models.TimeField()),
                ('golLocal', models.IntegerField()),
                ('golVisitante', models.IntegerField()),
                ('local', models.ForeignKey(related_name='equipo_local', to='equipo.Equipo')),
                ('visitante', models.ForeignKey(related_name='equipo_visitante', to='equipo.Equipo')),
            ],
        ),
    ]

from django.db import models
from django.shortcuts import get_object_or_404
from equipo.models import Equipo
from posiciones.models import Posiciones
from torneo.models import Torneo

# Create your models here.
class Partido(models.Model):
    torneo = models.ForeignKey(Torneo,related_name='torneoreferido')
    local = models.ForeignKey(Equipo,related_name='equipo_local')
    visitante = models.ForeignKey(Equipo,related_name='equipo_visitante')
    lugar = models.CharField(max_length=150)
    fecha = models.DateField()
    hora = models.TimeField()
    golLocal = models.IntegerField()
    golVisitante = models.IntegerField()

    def __str__(self):
        return "%s - %s" % (self.local, self.visitante)

    def save(self, *args, **kwargs):
        try:
            juego = Partido.objects.get(pk=self.pk)

            localRestar = Posiciones.objects.get(equipo=juego.local)
            visitanteRestar = Posiciones.objects.get(equipo=juego.visitante)

            if juego.golLocal > juego.golVisitante:
                localRestar.jugados = localRestar.jugados-1
                localRestar.ganados = localRestar.ganados-1
                localRestar.golFavor = localRestar.golFavor-juego.golLocal
                localRestar.golContra = localRestar.golContra-juego.golVisitante
                localRestar.diferencia = localRestar.diferencia - (juego.golLocal - juego.golVisitante)
                localRestar.puntos = localRestar.puntos-3
                localRestar.save()

                visitanteRestar.jugados = visitanteRestar.jugados-1
                visitanteRestar.perdidos = visitanteRestar.perdidos-1
                visitanteRestar.golFavor = visitanteRestar.golFavor-juego.golVisitante
                visitanteRestar.golContra = visitanteRestar.golContra-juego.golLocal
                visitanteRestar.diferencia = visitanteRestar.diferencia - (juego.golVisitante - juego.golLocal)
                visitanteRestar.save()
            elif juego.golLocal < juego.golVisitante:
                visitanteRestar.jugados = visitanteRestar.jugados-1
                visitanteRestar.ganados = visitanteRestar.ganados-1
                visitanteRestar.golFavor = visitanteRestar.golFavor-juego.golVisitante
                visitanteRestar.golContra = visitanteRestar.golContra-juego.golLocal
                visitanteRestar.diferencia = visitanteRestar.diferencia - (juego.golVisitante - juego.golLocal)
                visitanteRestar.puntos = visitanteRestar.puntos-3
                visitanteRestar.save()

                localRestar.jugados = localRestar.jugados-1
                localRestar.perdidos = localRestar.perdidos-1
                localRestar.golFavor = localRestar.golFavor-juego.golLocal
                localRestar.golContra = localRestar.golContra-juego.golVisitante
                localRestar.diferencia = localRestar.diferencia - (juego.golLocal - juego.golVisitante)
                localRestar.save()
            else:
                #restar partido jugado, empate, goles y punto a cada uno
                visitanteRestar.jugados = visitanteRestar.jugados-1
                visitanteRestar.empates = visitanteRestar.empates-1
                visitanteRestar.golFavor = visitanteRestar.golFavor-juego.golVisitante
                visitanteRestar.golContra = visitanteRestar.golContra-juego.golLocal
                visitanteRestar.puntos = visitanteRestar.puntos-1
                visitanteRestar.save()

                localRestar.jugados = localRestar.jugados-1
                localRestar.empates = localRestar.empates-1
                localRestar.golFavor = localRestar.golFavor-juego.golLocal
                localRestar.golContra = localRestar.golContra-juego.golVisitante
                localRestar.puntos = localRestar.puntos-1
                localRestar.save()

            self.procesar()
        except Partido.DoesNotExist:
            self.procesar()

        super(Partido,self).save(*args,**kwargs)

    def procesar(self):
        try:
            localPosicion = Posiciones.objects.get(equipo=self.local)
        except Posiciones.DoesNotExist:
            #nuevo objeto
            localPosicion = Posiciones(torneo=self.torneo, equipo=self.local, jugados=0, ganados=0, perdidos=0,
                                       empates=0, golFavor=0, golContra=0, diferencia=0, puntos=0)
            localPosicion.save()

        try:
            visitantePosicion = Posiciones.objects.get(equipo=self.visitante)
        except Posiciones.DoesNotExist:
            visitantePosicion = Posiciones(torneo=self.torneo, equipo=self.visitante, jugados=0, ganados=0, perdidos=0,
                                           empates=0, golFavor=0, golContra=0, diferencia=0, puntos=0)
            visitantePosicion.save()

        if self.golLocal > self.golVisitante: #gano local
            #localPosicion = get_object_or_404(Posiciones,equipo=self.local)
            localPosicion.jugados = localPosicion.jugados+1
            localPosicion.ganados = localPosicion.ganados+1
            localPosicion.golFavor = localPosicion.golFavor+self.golLocal
            localPosicion.golContra = localPosicion.golContra+self.golVisitante
            localPosicion.diferencia = localPosicion.diferencia + (self.golLocal - self.golVisitante)
            localPosicion.puntos = localPosicion.puntos+3
            localPosicion.save()

            #visitantePosicion = get_object_or_404(Posiciones,equipo=self.visitante)
            visitantePosicion.jugados = visitantePosicion.jugados+1
            visitantePosicion.perdidos = visitantePosicion.perdidos+1
            visitantePosicion.golFavor = visitantePosicion.golFavor+self.golVisitante
            visitantePosicion.golContra = visitantePosicion.golContra+self.golLocal
            visitantePosicion.diferencia = visitantePosicion.diferencia + (self.golVisitante - self.golLocal)
            visitantePosicion.save()
        elif self.golLocal < self.golVisitante: #gano visitante
            visitantePosicion.jugados = visitantePosicion.jugados+1
            visitantePosicion.ganados = visitantePosicion.ganados+1
            visitantePosicion.golFavor = visitantePosicion.golFavor+self.golVisitante
            visitantePosicion.golContra = visitantePosicion.golContra+self.golLocal
            visitantePosicion.diferencia = visitantePosicion.diferencia + (self.golVisitante - self.golLocal)
            visitantePosicion.puntos = visitantePosicion.puntos+3
            visitantePosicion.save()

            localPosicion.jugados = localPosicion.jugados+1
            localPosicion.perdidos = localPosicion.perdidos+1
            localPosicion.golFavor = localPosicion.golFavor+self.golLocal
            localPosicion.golContra = localPosicion.golContra+self.golVisitante
            localPosicion.diferencia = localPosicion.diferencia + (self.golLocal - self.golVisitante)
            localPosicion.save()
        else: #empate
            visitantePosicion.jugados = visitantePosicion.jugados+1
            visitantePosicion.empates = visitantePosicion.empates+1
            visitantePosicion.golFavor = visitantePosicion.golFavor+self.golVisitante
            visitantePosicion.golContra = visitantePosicion.golContra+self.golLocal
            visitantePosicion.puntos = visitantePosicion.puntos+1
            visitantePosicion.save()

            localPosicion.jugados = localPosicion.jugados+1
            localPosicion.empates = localPosicion.empates+1
            localPosicion.golFavor = localPosicion.golFavor+self.golLocal
            localPosicion.golContra = localPosicion.golContra+self.golVisitante
            localPosicion.puntos = localPosicion.puntos+1
            localPosicion.save()
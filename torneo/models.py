from django.db import models

# Create your models here.
class Torneo(models.Model):
    nombre = models.CharField(max_length=200)
    year = models.IntegerField()
    pais = models.CharField(max_length=100)
    participantes = models.IntegerField()

    def __str__(self):
        return "%s - %s" % (self.nombre,self.year)

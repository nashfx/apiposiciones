from django.contrib import admin

# Register your models here.
from torneo.models import Torneo

admin.site.register(Torneo)
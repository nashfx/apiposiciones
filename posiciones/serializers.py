from rest_framework import serializers
from .models import Posiciones
from equipo.serializers import EquipoSerializer

class PosicionesSerializer(serializers.ModelSerializer):
    equipoPosicion = EquipoSerializer(source='equipo')
    class Meta:
        model = Posiciones
        fields = ('id','torneo','equipoPosicion','jugados','ganados','empates','perdidos',
                  'golFavor','golContra','diferencia','puntos')
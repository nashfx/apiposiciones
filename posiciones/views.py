from django.shortcuts import render
from .serializers import PosicionesSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from posiciones.models import Posiciones
from torneo.models import Torneo

# Create your views here.
class PosicionesTorneo(APIView):
    def get(self, request, torneo, year, format=None):
        torneobuscado = Torneo.objects.filter(nombre=torneo,year=year)
        tabla = Posiciones.objects.filter(torneo=torneobuscado).order_by('-puntos','-diferencia','-ganados','-golFavor')
        serializer = PosicionesSerializer(tabla,many=True)
        return Response(serializer.data)
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posiciones', '0003_posiciones_year'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='posiciones',
            name='year',
        ),
    ]

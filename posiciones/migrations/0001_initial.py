# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('equipo', '0001_initial'),
        ('torneo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Posiciones',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ganados', models.IntegerField()),
                ('perdidos', models.IntegerField()),
                ('empates', models.IntegerField()),
                ('golFavor', models.IntegerField()),
                ('golContra', models.IntegerField()),
                ('diferencia', models.IntegerField()),
                ('puntos', models.IntegerField()),
                ('equipo', models.ForeignKey(related_name='equipo', to='equipo.Equipo')),
                ('torneo', models.ForeignKey(related_name='torneo', to='torneo.Torneo')),
            ],
        ),
    ]

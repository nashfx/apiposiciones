# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posiciones', '0002_posiciones_jugados'),
    ]

    operations = [
        migrations.AddField(
            model_name='posiciones',
            name='year',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]

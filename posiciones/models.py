from django.db import models
from equipo.models import Equipo
from torneo.models import Torneo

# Create your models here.
class Posiciones(models.Model):
    torneo = models.ForeignKey(Torneo,related_name="torneo")
    equipo = models.ForeignKey(Equipo,related_name="equipo")
    jugados = models.IntegerField()
    ganados = models.IntegerField()
    perdidos = models.IntegerField()
    empates = models.IntegerField()
    golFavor = models.IntegerField()
    golContra = models.IntegerField()
    diferencia = models.IntegerField()
    puntos = models.IntegerField()

    def __str__(self):
        return "%s: %s puntos" % (self.equipo, self.puntos)